#include<stdio.h>
struct Name
{
    char first_name[30];
    char last_name[30];
};
struct student
{
    int roll_no;
    struct Name n;
    char department[5];
    float fees;
    char section[2];
    int Totalmarks;
};
int main()
{
    struct student s[2];
    int i; 
    for(i=0;i<2;i++)
    {
        printf("Enter the details of student %d\n\n",i+1);
        printf("Enter the roll number\n");
        scanf("%d",&s[i].roll_no);
        printf("Enter the first name\n");
        scanf("%s",s[i].n.first_name);
        printf("Enter the last name\n");
        scanf("%s",s[i].n.last_name);
        printf("Enter the section\n");
        scanf("%s",s[i].section);
        printf("Enter the department\n");
        scanf("%s",s[i].department);
        printf("Enter the fees\n");
        scanf("%f",&s[i].fees);
        printf("Enter the total marks obtained out of 500\n");
        scanf("%d",&s[i].Totalmarks);
    }
    printf("\n......Student's details are \n");
    for(i=0;i<2;i++)
    {
        printf("Roll number =%d\n",s[i].roll_no);
        printf("First name  =%s\n",s[i].n.first_name);
        printf("Last name   =%s\n",s[i].n.last_name);
        printf("Section     =%s\n",s[i].section);
        printf("Department  =%s\n",s[i].department);
        printf("Fees        =%0.2f\n",s[i].fees);
        printf("Total marks =%d\n",s[i].Totalmarks);
    }
        if(s[0].Totalmarks > s[1].Totalmarks)
           {
                printf("the highest marks is scored by student 1 and the marks is %d\n" ,s[0].Totalmarks);
           }
        else if(s[1].Totalmarks > s[0].Totalmarks)
        {
                printf("the highest marks is scored by student 2 and the marks is %d\n" ,s[1].Totalmarks);
        }
        else
        {
            printf("the scores are equal\n");
        }
    return 0;
}
