#include<stdio.h>
#include<math.h>
int input()
{
    float a;
    printf("enter the number\n");
    scanf("%f" ,&a);
    return a;
}
float sum(float n)
{
    float i,sum=0,term;
    for(i=1;i<=n;i++)
    {
        term=pow(i,2); 
        sum=sum+term;
    }
    return sum;
}
void output(float sum)
{
    printf("the sum of series is 1^2+2^2+.....=%0.2f\n",sum);
}
int main()
{
    float x,y;
    x=input();
    y=sum(x);
    output(y);
    return 0;
}