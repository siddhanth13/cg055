#include<stdio.h>

void swap(int *num1,int *num2)
{
    int temp;
    temp=*num1;
    *num1=*num2;
    *num2=temp;
} 
int main()
{
    int num1,num2;
    printf("enter two number\n");
    scanf("%d%d" ,&num1,&num2);
    printf("\n....BEFORE SWAPPING.....\n");
    printf("num1 value is %d\n" ,num1);
    printf("num2 value is %d\n" ,num2);
    
    swap(&num1,&num2);
      
     printf("\n......AFTER SWAPPING......\n");
     printf("num1 value is %d\n" ,num1);
     printf("num2 value is %d\n" ,num2);
     return 0;
 }
