#include<stdio.h>
int main()
{
    int search_element,a[100],n,i,beg,mid,end;
    printf("enter the number of elements\n");
    scanf("%d" ,&n);
    printf("enter the input elements in sorted manner\n");
    for(i=0;i<n;i++)
        scanf("%d" ,&a[i]);
    
    printf("enter the element to search\n");
    scanf("%d" ,&search_element);
     beg=0;
     end=n-1;
     mid=(beg+end)/2;
    while ((beg<=end) && (a[mid]!=search_element))
    {
        if (search_element < a[mid])
            end = mid - 1;
        else 
            beg = mid + 1;
        mid = (beg + end) / 2;
    }
    if (a[mid] == search_element)
        printf("\nsearch element found at location %d", mid);
    else
        printf("\nsearch element doesn't exist");
    return 0;
}
