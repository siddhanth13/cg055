#include<stdio.h>
 void addition(int *a,int *b,int *s);
 void difference(int *a,int *b,int *d);
 void product(int *a,int *b,int *p);
 void quotient(int *a,int *b,int *q);
 void remaind(int *a,int *b,int *r);
 
 int main()
 {
     int num1,num2,sum,diff,pro,quo,rem;
     printf("enter two numbers to perform arithmatic operations\n");
     scanf("%d%d" ,&num1,&num2);
     addition(&num1,&num2,&sum);
     printf("the sum of two numbers is %d\n" ,sum);
     difference(&num1,&num2,&diff);
     printf("the difference of two numbers is %d\n" ,diff);
     product(&num1,&num2,&pro);
     printf("the product of two numbers is %d\n" ,pro);
     quotient(&num1,&num2,&quo);
     printf("the quotient of two numbers is %d\n" ,quo);
     remaind(&num1,&num2,&rem);
     printf("the remainder of two numbers is %d\n" ,rem); 
     return 0;
 }
 void addition(int *a,int *b,int *s)
 {
     *s=(*a)+(*b);
 }
 void difference(int *a,int *b,int *d)
 {
     *d=(*a)-(*b);
 }
 void product(int *a,int *b,int *p)
 {
     *p=(*a)*(*b);
 }
void quotient(int *a,int *b,int *q)
{
    *q=(*a)/(*b);
}
 void remaind(int *a,int *b,int *r)
 {
     *r=(*a)%(*b);
 }   
